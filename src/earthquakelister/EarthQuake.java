package earthquakelister;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EarthQuake {

	private String time;
	
	private float depth;
	
	private float magnitude;
	
	private String locality;
	
	private int mmi;
	
	private String quality;

	public EarthQuake(@JsonProperty(value = "time", required = true) String aTime,
					  @JsonProperty(value = "depth", required = true) float aDepth,
					  @JsonProperty(value = "magnitude", required = true) float aMagnitude,
					  @JsonProperty(value = "locality", required = true) String aLocality,
					  @JsonProperty(value = "mmi", required = true) int aMMi,
					  @JsonProperty(value = "quality", required = true) String aQuality) {
		time = aTime;
		depth = aDepth;
		magnitude = aMagnitude;
		locality = aLocality;
		mmi = aMMi;
		quality = aQuality;
	}

	public String getTime() {
		return time;
	}

	public float getDepth() {
		return depth;
	}

	public float getMagnitude() {
		return magnitude;
	}

	public String getLocality() {
		return locality;
	}

	public int getMmi() {
		return mmi;
	}

	public String getQuality() {
		return quality;
	}

	public boolean isGoodQualityOrBetter() {
		return "good".equals(quality) || "best".equals(quality);
	}
}
