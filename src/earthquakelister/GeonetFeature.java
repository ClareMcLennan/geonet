package earthquakelister;

public class GeonetFeature {

	private EarthQuake properties;

	public EarthQuake getProperties() {
		return properties;
	}

	public void setProperties(EarthQuake properties) {
		this.properties = properties;
	}
}
