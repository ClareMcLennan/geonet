package earthquakelister;

import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;


public class EarthQuakeWriter {

	private final List<EarthQuake> earthQuakes;

	private final PrintStream outputStream;

	public EarthQuakeWriter(List<EarthQuake> earthQuakeList) {
		earthQuakes = earthQuakeList;
		
		outputStream = System.out;
	}

	public EarthQuakeWriter(List<EarthQuake> earthQuakeList, PrintStream stream) {
		earthQuakes = earthQuakeList;
		
		outputStream = stream;
	}

	public void writeQuakes() {
		if (earthQuakes == null) {
			printNoQuakesMessage();
		}
		
		if (earthQuakes.size() > 99) {
			printTooManyQuakesMessage();
		}
		
		outputStream.println();
		printTitleStrings();
		
		List<EarthQuake> goodQualityQuakes = getGoodQualityQuakes();
		
		for (EarthQuake quake : goodQualityQuakes) {
			printEarthQuake(quake);
		}
		
		outputStream.println();
		outputStream.println("Average magnitude is " + getAverageMagnitude(goodQualityQuakes));
		outputStream.println("Average depth is " + getAverageDepth(goodQualityQuakes));
	}

	double getAverageMagnitude(List<EarthQuake> goodQualityQuakes) {
		return goodQualityQuakes.stream().mapToDouble(EarthQuake::getMagnitude).average().getAsDouble();
	}

	double getAverageDepth(List<EarthQuake> goodQualityQuakes) {
		return goodQualityQuakes.stream().mapToDouble(EarthQuake::getDepth).average().getAsDouble();
	}
	
	private void printTitleStrings() {
		outputStream.println("TIME, LOCALITY, MAGNITUDE, DEPTH, MMI");
	}

	private void printEarthQuake(EarthQuake eq) {
		outputStream.println(eq.getTime() + "," 
						 + eq.getLocality() + ","
 	 					 + eq.getMagnitude() + ","
 	 					 + eq.getDepth() + ","
 	 					 + eq.getMmi());
	}

	List<EarthQuake> getGoodQualityQuakes() {
		return earthQuakes.stream().filter(eq -> eq.isGoodQualityOrBetter()).collect(Collectors.toList());
	}

	
	private void printTooManyQuakesMessage() {
		outputStream.println();
		outputStream.println("WARNING:  You are only seeing the latest quakes not all those that occured in the last year");
	}

	private void printNoQuakesMessage() {
		outputStream.println("There were no quakes of good quality that match your criteria");
	}
}
