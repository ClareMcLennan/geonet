package earthquakelister;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GeonetEarthQuakeRetreiver {

	private final static String GEONET_URL = "http://api.geonet.org.nz/quake?MMI=";
	
	private final int minMmi;
	
	public GeonetEarthQuakeRetreiver(int userMinMmi) {
		minMmi = userMinMmi;
	}
	
	public List<EarthQuake> getQuakes() throws IOException {
		URL geonetUrl = new URL(GEONET_URL + Integer.toString(minMmi));
		
		try (InputStream stream = geonetUrl.openStream()) {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			
			GeonetFile file = objectMapper.readValue(stream, GeonetFile.class);
			
			return Arrays.stream(file.getFeatures()).map(feature -> feature.getProperties()).collect(Collectors.toList());
		}
	}
	
	public static void main(String[] args) throws IOException {
		GeonetEarthQuakeRetreiver retreiver = new GeonetEarthQuakeRetreiver(5);
		
		retreiver.getQuakes();
	}
}
