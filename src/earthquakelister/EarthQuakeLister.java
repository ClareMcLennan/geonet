package earthquakelister;

import java.io.IOException;
import java.util.List;

public class EarthQuakeLister {
	private final static int DEFAULT_MMI = 3;

	public static void main(String[] args) throws IOException {

		int minMMI = DEFAULT_MMI;
		
		if (args.length > 0) {
			try {
				minMMI = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException exception){
				System.out.println("Error: Minimum MMI (first parameter) should be an integer between -1 and 8.");
				System.out.println("Exiting without retrieving quakes.");
				System.exit(1);
			}
		}
		
		System.out.println("Earth quakes occuring in the last year with quality at least good and MMI greater or equal to " + minMMI);

		GeonetEarthQuakeRetreiver parser = new GeonetEarthQuakeRetreiver(minMMI);
		
		List<EarthQuake> allQuakes = parser.getQuakes();
		
		EarthQuakeWriter writer = new EarthQuakeWriter(allQuakes);
		
		writer.writeQuakes();
			
	}
}
