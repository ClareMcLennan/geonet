package earthquakelister;

public class GeonetFile {

	private GeonetFeature[] features;

	public GeonetFeature[] getFeatures() {
		return features;
	}

	public void setFeatures(GeonetFeature[] features) {
		this.features = features;
	}
}
