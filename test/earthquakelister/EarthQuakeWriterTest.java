package earthquakelister;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class EarthQuakeWriterTest {
 
	List<EarthQuake> earthQuakes = new LinkedList<>();

	ByteArrayOutputStream output = new ByteArrayOutputStream();
	
	PrintStream stream = new PrintStream(output);
	
	@Test
	public void testDepthAndMagnitudeAverages() {
		earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 27.2f, 4.3f, "5.5 km north of Cheviot", 5, "good"));
		earthQuakes.add(new EarthQuake("2017-03-04T07:27:38.483Z", 55.3f, 4.7f, "5,5 km south-east of Seddon", 5, "good"));
		earthQuakes.add(new EarthQuake("2017-03-03T02:49:13.970Z", 13.2f, 5.6f, "20 km south-west of Cheviot", 5, "good"));
		
		EarthQuakeWriter writer = new EarthQuakeWriter(earthQuakes);
		
		assertEquals(31.9f, writer.getAverageDepth(earthQuakes), .1f);
		assertEquals(4.86f, writer.getAverageMagnitude(earthQuakes), .1f);
	}
	
	@Test
	public void testGoodAndBestQualityQuakesAreIncluded() {
		earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 27.2f, 4.3f, "5.5 km north of Cheviot", 5, "good"));
		earthQuakes.add(new EarthQuake("2017-03-04T07:27:38.483Z", 55.3f, 4.7f, "5,5 km south-east of Seddon", 5, "best"));
		
		EarthQuakeWriter writer = new EarthQuakeWriter(earthQuakes);
		assertEquals(2, writer.getGoodQualityQuakes().size());
	}

	@Test
	public void testPoorQualityQuakesAreNotIncluded() {
		earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 27.2f, 4.3f, "5.5 km north of Cheviot", 5, "caution"));
		earthQuakes.add(new EarthQuake("2017-03-04T07:27:38.483Z", 55.3f, 4.7f, "5,5 km south-east of Seddon", 5, "deleted"));
		
		EarthQuakeWriter writer = new EarthQuakeWriter(earthQuakes);
		assertEquals(0, writer.getGoodQualityQuakes().size());
	}
	
	@Test
	public void testOutputAveragesOnlyIncludeGoodQuakes() {
		earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 27.2f, 4.3f, "5.5 km north of Cheviot", 5, "good"));
		earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 33f, 4.23f, "5.5 km north of Cheviot", 5, "caution"));
		earthQuakes.add(new EarthQuake("2017-03-04T07:27:38.483Z", 22f, 4.79f, "5,5 km south-east of Seddon", 5, "deleted"));
		
		EarthQuakeWriter writer = new EarthQuakeWriter(earthQuakes, stream);
		writer.writeQuakes();
		
		assertTrue(output.toString().contains("Average magnitude is 4.3"));
		assertTrue(output.toString().contains("Average depth is 27.2"));
	}

	@Test
	public void testWarningAbout100QuakesCalledEvenIfTheyArePoorQuality() {
		earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 27.2f, 4.3f, "5.5 km north of Cheviot", 5, "good"));
		for (int index = 0; index < 99; index++) {
			earthQuakes.add(new EarthQuake("2017-03-09T16:00:58.020Z", 33f, 4.23f, "5.5 km north of Cheviot", 5, "caution"));
		}
		
		EarthQuakeWriter writer = new EarthQuakeWriter(earthQuakes, stream);
		writer.writeQuakes();
		
		assertTrue(output.toString().contains("WARNING:  You are only seeing the latest quakes"));
	}

}
